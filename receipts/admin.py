from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

@admin.register(Receipt)
class Reciept(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
    

@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
    )


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )




# Register your models here.
